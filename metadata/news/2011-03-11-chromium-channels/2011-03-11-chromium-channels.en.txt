Title: Three chromium packages instead of just one
Author: Elias Pipping <pipping@exherbo.org>
Content-Type: text/plain
Posted: 2011-03-11
Revision: 1
News-Item-Format: 1.0

Instead of net-www/chromium there are now three packages: net-www/chromium-{stable,beta,dev}.
None of these will be masked by default. They cannot be installed in parallel. Choose wisely.
