Title: Chromium PDF plugin now in separate exheres
Author: Kim Højgaard-Hansen <kimrhh@exherbo.org>
Content-Type: text/plain
Posted: 2011-10-13
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: net-www/chromium-dev[pdf(-)]

The PDF plugin for chromium is now installed using a separate exheres
instead of the pdf option. The new exheres is chromium-dev-pdf-plugin.
