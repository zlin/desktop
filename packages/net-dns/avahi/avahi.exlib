# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2010-2013 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=lathiat release=v${PV} suffix=tar.gz ] \
    systemd-service \
    freedesktop-desktop \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]

export_exlib_phases src_prepare src_configure src_compile src_install pkg_postinst pkg_postrm

SUMMARY="mDNS discovery agent"
DESCRIPTION="
Avahi is a framework for Multicast DNS Service Discovery (mDNS/DNS-SD a.k.a. Zeroconf)
on Linux. It allows programs to publish and discover services running on a local
network with no specific configuration. For example, you can plug into a network
and instantly find printers to print to, files to look at, and people to talk to.
"

REMOTE_IDS="freecode:${PN}"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    autoipd [[ description = [ Build and install the IPv4LL (RFC3927) network address configuration daemon ] ]]
    dbus
    dns_sd [[ description = [ Libdns_sd compatibility layer for avahi ] requires = [ dbus ] ]]
    doc
    gdbm
    gobject-introspection
    gtk
    gtk3 [[ description = [ Use the new GTK+-3 toolkit ] ]]
    ( linguas: ach ar bg ca cs da de el en_AU en_CA en_GB en_NZ eo es et fa fi fo fr gl he hu id it
               ja ko lv ms nl oc pl pt_BR ro ru sk sl sr sr@latin sv tr uk zh_CN zh_TW )
    ( libc: glibc )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35.0]
        virtual/pkg-config[>=0.20]
        doc? ( app-doc/doxygen )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.7] )
    build+run:
        dev-libs/expat
        dev-libs/glib:2[>=2.4]
        dev-libs/libdaemon[>=0.14]
        sys-libs/libcap
        dbus? ( sys-apps/dbus[>=0.34] )
        gdbm? ( sys-libs/gdbm )
        gtk? ( x11-libs/gtk+:2[>=2.14] )
        gtk3? ( x11-libs/gtk+:3 )
        group/avahi
        group/netdev
        user/avahi
    run:
        autoipd? (
            group/avahi-autoipd
            user/avahi-autoipd
        )
    recommendation:
        libc:glibc? ( net-dns/nss-mdns )    [[ note = [ only recommended on glibc because only glibc supports nss modules ] ]]
"

DEFAULT_SRC_INSTALL_PARAMS=( py_compile=true )
DEFAULT_SRC_INSTALL_EXTRA_PREFIXES=( docs/ )

AT_M4DIR=( common )

avahi_src_configure() {
    local opts=(
        --localstatedir=/var
        --enable-glib
        --enable-libdaemon
        --disable-compat-howl
        --disable-manpages
        --disable-mono
        --disable-monodoc
        --disable-pygtk
        --disable-python
        --disable-python-dbus
        --disable-qt3
        --disable-qt4
        --disable-static
        --disable-xmltoman
        --with-distro=none
        --with-xml=expat
        --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
        $(option_enable autoipd)
        $(option_enable dbus)
        $(option_enable dns_sd compat-libdns_sd)
        $(option_enable doc doxygen-doc)
        $(option_enable gdbm)
        $(option_enable gobject-introspection introspection)
        $(option_enable gtk)
        $(option_enable gtk3)
    )

    econf ${opts[@]}
}

avahi_src_prepare() {
    # find prefixed pkg-config
    # TODO: fix upstream
    edo sed \
        -e "s/^\(AC_CHECK_PROG(.*\)pkg-config/\1${PKG_CONFIG}/" \
        -i configure.ac

    # fix documentation path
    edo sed \
        -e "s:\\.\\./\\.\\./\\.\\./doc/avahi-docs/html/:../../../doc/${PNVR}/html/:" \
        -i doxygen_to_devhelp.xsl

    autotools_src_prepare
}

avahi_src_compile() {
    default

    option doc && emake avahi.devhelp
}

avahi_src_install() {
    default

    edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/avahi/

    if option autoipd ; then
        insinto /usr/$(exhost --target)/lib/rcscripts/net
        doins "${FILES}"/autoipd.sh
    fi

    if option doc ; then
        insinto /usr/share/doc/${PNV}/html
        doins doxygen/html/*

        # requires doxygen docs
        insinto /usr/share/devhelp/books/avahi
        doins avahi.devhelp
    fi

    edo rmdir "${IMAGE}"/run
}

avahi_pkg_postinst() {
    if option gtk || option gtk3 ; then
        freedesktop-desktop_pkg_postinst
    fi

    if option autoipd ; then
        elog "To use avahi-autoipd to configure your interfaces with IPv4LL (RFC3927)"
        elog "addresses, and configure your network interface to use autoipd."
    fi

    if option dbus ; then
        elog "If this is your first install of avahi, please reload your dbus config."
    fi
}

avahi_pkg_postrm() {
    if option gtk || option gtk3 ; then
        freedesktop-desktop_pkg_postrm
    fi
}

