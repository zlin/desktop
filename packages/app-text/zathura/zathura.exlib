# Copyright 2012 Johannes Nixdorf <mixi@exherbo.org>
# Copyright 2010 Jonathan Dahan <jedahan@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require pwmt meson

SUMMARY="Highly customizable and functional PDF viewer based on poppler/GTK+"

LICENCES="ZLIB"
SLOT="0"
MYOPTIONS="
    sqlite [[ description = [ Support sqlite databases ] ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool
        virtual/pkg-config
        dev-python/Sphinx[python_abis:*(-)?] [[ description = [ Necessary to build man-pages ] ]]
    build+run:
        dev-libs/glib:2[>=2.32]
        sys-apps/file                       [[ description = [ Support mimetype detection with libmagic ] ]]
        x11-libs/cairo
        x11-libs/girara[>=0.2.9]
        x11-libs/gtk+:3[>=3.6]
        sqlite? ( dev-db/sqlite:3[>=3.5.9] )
    test:
        dev-libs/check
    recommendation:
        virtual/zathura-pdf                 [[ description = [ Used to view PDF documents ] ]]
    suggestion:
        text-plugins/zathura-djvu           [[ description = [ Used to view djvu documents ] ]]
        text-plugins/zathura-ps             [[ description = [ Used to view postscript documents ] ]]
        text-plugins/zathura-cb             [[ description = [ Used to view comic books ] ]]
"

# they require a running x server
RESTRICT="test"

BUGS_TO="jedahan@gmail.com"

MESON_SRC_CONFIGURE_PARAMS=(
    -Denable-magic=true
)

MESON_SRC_CONFIGURE_OPTION_ENABLES=(
    sqlite
)

