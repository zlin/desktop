# Copyright 2009-2011 Elias Pipping <pipping@exherbo.org>
# Copyright 2009 Heiko Przybyl <zuxez@cs.tu-berlin.de>
# Distributed under the terms of the GNU General Public License v2

require chromium

DESCRIPTION+="
Chromium has three release channels: Stable, Beta, and Dev. This package corresponds to the Stable
channel.
"

PLATFORMS="~amd64 ~x86"

RESTRICT="test"

DEPENDENCIES+="
    build+run:
        !net-www/chromium-stable-flash-plugin [[
            description = [ Chrome binary plugins don't contain flash anymore ]
            resolution = uninstall-blocked-before
        ]]
        !net-www/chromium-stable-pdf-plugin [[
            description = [ Chromium now provides libpdf ]
            resolution = uninstall-blocked-before
        ]]
    suggestion:
        net-www/chromium-stable-widevine-plugin [[ description = [ Content Decryption Module plugin required for e.g. Netflix ] ]]
"

# Overview with URLs for distro-specific patches
# https://chromium.googlesource.com/chromium/src/+/master/docs/linux_chromium_packages.md
DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/chromium-widevine-assertion.patch
    "${FILES}"/chromium-widevine-revision.patch
    "${FILES}"/chromium-webrtc-r0.patch
    "${FILES}"/chromium-math.h-r0.patch
    "${FILES}"/chromium-stdint.patch
    "${FILES}"/chromium-swiftshader-default-visibility.patch
    "${FILES}"/chromium-add_missing_stdint_include.patch
    "${FILES}"/chromium-fix_harfbuzz_2.patch
    "${FILES}"/chromium-remove_sysroot_related_options.patch
    "${FILES}"/gcc-patches/chromium-rename_snap_container_data.patch
    "${FILES}"/gcc-patches/chromium-fix_blink_build_error.patch
    "${FILES}"/gcc-patches/chromium-fix_struct_init.patch
)

CHROMIUM_KEEPLIBS=(
    base/third_party/dmg_fp
    base/third_party/dynamic_annotations
    base/third_party/icu
    base/third_party/nspr
    base/third_party/superfasthash
    base/third_party/symbolize
    base/third_party/valgrind
    base/third_party/xdg_mime
    base/third_party/xdg_user_dirs
    buildtools/third_party/libc++
    buildtools/third_party/libc++abi
    chrome/third_party/mozilla_security_manager
    courgette/third_party
    net/third_party/http2
    net/third_party/mozilla_security_manager
    net/third_party/nss
    net/third_party/quic
    net/third_party/spdy
    net/third_party/uri_template
    third_party/WebKit
    third_party/abseil-cpp
    third_party/analytics
    third_party/angle
    third_party/angle/src/common/third_party/base
    third_party/angle/src/common/third_party/smhasher
    third_party/angle/src/third_party/compiler
    third_party/angle/src/third_party/libXNVCtrl
    third_party/angle/src/third_party/trace_event
    third_party/angle/third_party/glslang
    third_party/angle/third_party/spirv-headers
    third_party/angle/third_party/spirv-tools
    third_party/angle/third_party/vulkan-headers
    third_party/angle/third_party/vulkan-loader
    third_party/angle/third_party/vulkan-tools
    third_party/angle/third_party/vulkan-validation-layers
    third_party/apple_apsl
    third_party/blink
    third_party/boringssl
    third_party/boringssl/src/third_party/fiat
    third_party/breakpad
    third_party/breakpad/breakpad/src/third_party/curl
    third_party/brotli
    third_party/cacheinvalidation
    third_party/catapult
    third_party/catapult/common/py_vulcanize/third_party/rcssmin
    third_party/catapult/common/py_vulcanize/third_party/rjsmin
    third_party/catapult/third_party/polymer
    third_party/catapult/tracing/third_party/d3
    third_party/catapult/tracing/third_party/gl-matrix
    third_party/catapult/tracing/third_party/jszip
    third_party/catapult/tracing/third_party/mannwhitneyu
    third_party/catapult/tracing/third_party/oboe
    third_party/catapult/tracing/third_party/pako
    third_party/ced
    third_party/cld_3
    third_party/crashpad
    third_party/crashpad/crashpad/third_party/zlib
    third_party/crc32c
    third_party/cros_system_api
    third_party/devscripts
    third_party/dom_distiller_js
    third_party/fips181
    third_party/flatbuffers
    third_party/flot
    third_party/freetype
    third_party/glslang-angle
    third_party/google_input_tools
    third_party/google_input_tools/third_party/closure_library
    third_party/google_input_tools/third_party/closure_library/third_party/closure
    third_party/googletest
    third_party/hunspell
    third_party/iccjpeg
    third_party/icu
    third_party/inspector_protocol
    third_party/jinja2
    third_party/jstemplate
    third_party/khronos
    third_party/leveldatabase
    third_party/libXNVCtrl
    third_party/libaddressinput
    third_party/libaom
    third_party/libaom/source/libaom/third_party/vector
    third_party/libaom/source/libaom/third_party/x86inc
    third_party/libjingle
    third_party/libphonenumber
    third_party/libsecret
    third_party/libsrtp
    third_party/libsync
    third_party/libudev
    third_party/libwebm
    third_party/libxml/chromium
    third_party/libyuv
    third_party/llvm
    third_party/lss
    third_party/lzma_sdk
    third_party/markupsafe
    third_party/mesa
    third_party/metrics_proto
    third_party/modp_b64
    third_party/node
    third_party/node/node_modules/polymer-bundler/lib/third_party/UglifyJS2
    third_party/openmax_dl
    third_party/ots
    third_party/perfetto
    third_party/pdfium
    third_party/pdfium/third_party/agg23
    third_party/pdfium/third_party/base
    third_party/pdfium/third_party/bigint
    third_party/pdfium/third_party/freetype
    third_party/pdfium/third_party/lcms
    third_party/pdfium/third_party/libopenjpeg20
    third_party/pdfium/third_party/libpng16
    third_party/pdfium/third_party/libtiff
    third_party/pdfium/third_party/skia_shared
    third_party/ply
    third_party/polymer
    third_party/protobuf
    third_party/protobuf/third_party/six
    third_party/pyjson5/src/json5
    third_party/qcms
    third_party/rnnoise
    third_party/s2cellid
    third_party/sfntly
    third_party/skia
    third_party/skia/third_party/gif
    third_party/skia/third_party/skcms
    third_party/skia/third_party/vulkan
    third_party/smhasher
    third_party/spirv-headers
    third_party/spirv-tools-angle
    third_party/sqlite
    third_party/swiftshader
    third_party/swiftshader/third_party/llvm-subzero
    third_party/swiftshader/third_party/subzero
    third_party/tcmalloc
    third_party/unrar
    third_party/usrsctp
    third_party/vulkan
    third_party/vulkan-validation-layers
    third_party/web-animations-js
    third_party/webdriver
    third_party/webrtc
    third_party/webrtc/common_audio/third_party/fft4g
    third_party/webrtc/common_audio/third_party/spl_sqrt_floor
    third_party/webrtc/modules/third_party/fft
    third_party/webrtc/modules/third_party/g711
    third_party/webrtc/modules/third_party/g722
    third_party/webrtc/rtc_base/third_party/base64
    third_party/webrtc/rtc_base/third_party/sigslot
    third_party/widevine
    third_party/woff2
    third_party/zlib/google
    url/third_party/mozilla
    v8/src/third_party/valgrind
    v8/src/third_party/utf8-decoder
    v8/third_party/inspector_protocol
    v8/third_party/v8
)

# TODO: keep for GN builds
CHROMIUM_KEEPLIBS+=(
    third_party/adobe
    third_party/speech-dispatcher
    third_party/usb_ids
    third_party/xdg-utils
    third_party/yasm/run_yasm.py
    tools/gn/base/third_party/icu
)

# last checked: 66.0.3359.139
# vp9_impl.cc:720:25: error: 'const struct vpx_codec_cx_pkt::<unnamed union>::<unnamed>' has no member named 'height'
CHROMIUM_KEEPLIBS+=(
    third_party/libvpx
    third_party/libvpx/source/libvpx/third_party/x86inc
)

# libvpx: see keeplibs above
# icu: causes build error as of 69.0.3497.81 (https://bugs.gentoo.org/661880)
CHROMIUM_SYSTEM_LIBS="
    ffmpeg
    flac
    fontconfig
    libdrm
    libevent
    libjpeg
    libpng
    libwebp
    libxml
    libxslt
    openh264
    opus
    re2
    snappy
    yasm
    zlib
"

src_install() {
    chromium_src_install

    # Required because we don't use system icu
    insinto /opt/${MY_PN}
    doins icudtl.dat
}

