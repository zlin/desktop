# Copyright 2009 Richard Brown <rbrown@exherbo.org>
# Copyright 2011 Mike Kazantsev <mk.fraggod@gmail.com>
# Copyright 2012 Wouter van Kesteren <woutershep@gmail.com>
# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require spidermonkey python [ blacklist=3 has_lib=false has_bin=false multibuild=false ]

DOWNLOADS="https://ftp.mozilla.org/pub/firefox/releases/${PV}esr/source/firefox-${PV}esr.source.tar.bz2"

LICENCES="|| ( MPL-1.1 MPL-2.0 GPL-2 GPL-3 LGPL-2.1 LGPL-3 )"
SLOT="38"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-arch/zip
        virtual/pkg-config
    build+run:
        dev-libs/icu:=[>=58.2]
        dev-libs/libffi[>=3.1]
        dev-libs/nspr[>=4.13.1]
        sys-libs/readline:=
"

WORK=${WORKBASE}/mozilla-esr${SLOT}/js/src

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=docdir
    --hates=datarootdir
    --enable-intl-api
    --enable-jemalloc
    --enable-readline
    --enable-system-ffi
    --enable-threadsafe
    --enable-ui-locale=en_US
    --disable-static
    --with-system-icu
    --with-system-nspr
)

pkg_setup() {
    # spidermonkey build system requires that SHELL is always set.
    # It's missing sometimes in chroot environments, so force it here.
    export SHELL=/bin/sh
}

src_prepare() {
    default
    edo sed -e 's/\[:space:]/[&]/' -i configure
    cd "${WORKBASE}/mozilla-esr${SLOT}"
    expatch "${FILES}"/${SLOT}/*
}

src_install() {
    default
    edo rm "${IMAGE}"usr/$(exhost --target)/lib/libjs_static.ajs
}

src_test() {
    esandbox allow_net "unix:${TEMP%/}/pymp-*/listener-*"
    default
    esandbox disallow_net "unix:${TEMP%/}/pymp-*/listener-*"
}

