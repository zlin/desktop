# Copyright 2010 Joonas Sarajärvi <muepsj@gmail.com>
# Copyright 2012 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of GNU General Public License v2

if ever is_scm; then
    SCM_REPOSITORY="https://anongit.freedesktop.org/git/${PN}/${PN}.git"
    require scm-git autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
else
    DOWNLOADS="https://www.freedesktop.org/software/${PN}/${PNV}.tar.xz"
fi

require gtk-icon-cache udev-rules vala [ vala_dep=true with_opt=true option_name=vapi ]
require systemd-service test-dbus-daemon
require option-renames [ renames=[ 'policykit polkit' 'vala vapi' ] ]

SUMMARY="Provides mobile 3G support for NetworkManager"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/${PN}"

BUGS_TO="muepsj@gmail.com"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    mbim [[ description = [ Support for the Mobile Interface Broadband Model (MBIM) protocol ] ]]
    polkit
    qmi [[ description = [ Support for the Qualcomm MSM Interface (QMI) protocol ] ]]
    systemd [[ description = [ Integrate with systemd-journald and systemd suspend/resume features ] ]]
    vapi [[ requires = [ gobject-introspection ] ]]
    ( linguas: cs de fr fur hu id it pl pt_BR sk sv tr uk )
"

# tests want to connect to systemd journal
# and various other things (last checked: 1.4.12)
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        dev-libs/glib:2[>=2.36]
        gnome-desktop/libgudev[>=147]
        sys-apps/dbus
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.6] )
        mbim? ( net-libs/libmbim[>=1.16.0] )
        polkit? ( sys-auth/polkit:1[>=0.97] )
        qmi? ( net-libs/libqmi[>=1.20.0][mbim?] )
        systemd? ( sys-apps/systemd[>=209] )
    recommendation:
        net-misc/mobile-broadband-provider-info
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --prefix=/usr
    --exec_prefix=/usr/$(exhost --target)
    --includedir=/usr/$(exhost --target)/include
    --enable-nls
    --disable-more-warnings
    --disable-static
    --with-dbus-sys-dir=/usr/share/dbus-1/system.d
    --with-udev
    --with-udev-base-dir=${UDEVDIR}
    --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
    'vapi vala'
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    polkit
    'mbim'
    'qmi'
    'systemd systemd-journal'
    'systemd systemd-suspend-resume'
)

